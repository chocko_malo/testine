// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDc09Gef1zvbUswu3gc-09cotvqoPiJidU",
    authDomain: "authine-181af.firebaseapp.com",
    projectId: "authine-181af",
    storageBucket: "authine-181af.appspot.com",
    messagingSenderId: "786030531143",
    appId: "1:786030531143:web:98ac8f1f0e67fce8fb9762"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
