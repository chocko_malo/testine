import { Injectable } from '@angular/core';

import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class GetCharactersService {
  
  baseURL: string = "https://rickandmortyapi.com/api/character/";
 
  constructor(private http: HttpClient) {
  }
 
  getChars(){
    return this.http.get(this.baseURL+"?page=1")
  }

  getOne(id:string){
    return this.http.get(this.baseURL+id)
  }

}
