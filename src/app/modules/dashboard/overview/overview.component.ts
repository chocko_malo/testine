import { Component, OnInit } from '@angular/core';
import { GetCharactersService } from 'src/app/shared/services/get-characters.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  data =  [];
  constructor(private getCharactersService:GetCharactersService) { }

  ngOnInit(): void {
    this.getCharacters();
  }

  getCharacters(){
    this.getCharactersService.getChars().
    subscribe({
      next: (res: any) => this.data = res.results ,
      error: (err) => err
    })
  }

}
