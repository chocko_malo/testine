import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { OverviewComponent } from './overview/overview.component';
import { DetailsComponent } from './details/details.component';
import { MapComponent } from './map/map.component';
import { TestComponent } from './test/test.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
     {
       path: 'overview',
       component: OverviewComponent
     },
     {
       path: 'overview/:id',
       component: DetailsComponent
     },
     {
      path: 'map',
      component: MapComponent
    },
     {
      path: 'test',
      component: TestComponent
    },
     {
      path: 'admin',
      component: AdminComponent
    },
    ]
  },
  {  path: '**', redirectTo: 'overview' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
