import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetCharactersService } from 'src/app/shared/services/get-characters.service';

interface Chapter {
  episode:[];
}

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  searchTerm: string ='';
  data:Chapter ={
    episode: []
  };
  constructor(private route: ActivatedRoute,
    private getCharactersService:GetCharactersService) {
        this.getCharacter(this.route.snapshot.params['id']);
      
    }

  ngOnInit(): void {

  }

  getCharacter(id:string){
    this.getCharactersService.getOne(id).
    subscribe({
      next: (res: any) => this.data = res,
      error: (err) => err
    })
  }

}
