import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumb: string[]=[];
  constructor(private router: Router, private cdr: ChangeDetectorRef) {
  }
  
  ngOnInit(): void {
  }
  ngAfterViewChecked(): void {
    let arrayBredcrumb = this.router.url.split('/');
    this.breadcrumb = arrayBredcrumb.slice(1, arrayBredcrumb.length);
    this.cdr.detectChanges();

  }
  bredcrumFn(){
    return this.breadcrumb.splice(0, this.breadcrumb.length-1).reduce((accumulator, currentValue) => accumulator +'/'+ currentValue,'')
  }
}
