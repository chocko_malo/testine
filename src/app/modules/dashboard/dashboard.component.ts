import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  routes = [
    {route:'overview', text:'Personajes'},
    {route:'map', text:'HighMaps'},
    {route:'test', text:'test'},
    {route:'admin', text:'admin'},
  ]
  constructor(public authService: AuthService) {}
  ngOnInit(): void {}
}