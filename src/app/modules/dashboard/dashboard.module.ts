import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { GetCharactersService } from 'src/app/shared/services/get-characters.service';
import { CommonModule } from '@angular/common';
import { OverviewComponent } from './overview/overview.component';
import { DetailsComponent } from './details/details.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from 'src/app/shared/pipes/filter.pipe';
import { MapComponent } from './map/map.component';
import { TestComponent } from './test/test.component';
import { AdminComponent } from './admin/admin.component';
import { HighchartsChartModule } from 'highcharts-angular';



@NgModule({
  declarations: [
    DashboardComponent,
    BreadcrumbComponent,
    OverviewComponent,
    DetailsComponent,
    FilterPipe,
    MapComponent,
    TestComponent,
    AdminComponent
  ],
  imports: [
    DashboardRoutingModule,
    CommonModule,
    FormsModule,
    HighchartsChartModule
  ],
  providers: [GetCharactersService],
})
export class DashboardModule { }
